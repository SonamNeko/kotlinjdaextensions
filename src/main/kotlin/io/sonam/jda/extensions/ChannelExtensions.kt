package io.sonam.jda.extensions

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.requests.RestAction

fun MessageChannel.buildMessage(init: MessageBuilder.() -> Unit): RestAction<Message> {
    return this.sendMessage(io.sonam.jda.extensions.buildMessage(init))
}

fun MessageChannel.buildEmbed(init: EmbedBuilder.() -> Unit): RestAction<Message> {
    return this.sendMessage(io.sonam.jda.extensions.buildEmbed(init))
}
