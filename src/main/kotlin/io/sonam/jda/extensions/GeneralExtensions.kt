package io.sonam.jda.extensions

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageEmbed

inline fun buildJda(init: JDABuilder.() -> Unit): JDA {
    val builder = JDABuilder(); builder.init(); return builder.buildAsync()
}

inline fun buildEmbed(init: EmbedBuilder.() -> Unit): MessageEmbed {
    val builder = EmbedBuilder(); builder.init(); return builder.build()
}

inline fun buildMessage(init: MessageBuilder.() -> Unit): Message {
    val builder = MessageBuilder(); builder.init(); return builder.build()
}


