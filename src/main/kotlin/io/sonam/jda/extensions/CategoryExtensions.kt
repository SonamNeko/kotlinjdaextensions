package io.sonam.jda.extensions

import net.dv8tion.jda.core.entities.Category
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.requests.RequestFuture
import net.dv8tion.jda.core.requests.restaction.ChannelAction

fun Category.buildTextChannel(name: String, init: ChannelAction.() -> Unit): ChannelAction {
    val builder = this.createTextChannel(name); builder.init(); return builder
}

fun Category.buildVoiceChannel(name: String, init: ChannelAction.() -> Unit): ChannelAction {
    val builder = this.createVoiceChannel(name); builder.init(); return builder
}
