package io.sonam.jda.handler

import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import net.dv8tion.jda.client.events.call.CallCreateEvent
import net.dv8tion.jda.client.events.call.CallDeleteEvent
import net.dv8tion.jda.client.events.call.GenericCallEvent
import net.dv8tion.jda.client.events.call.update.CallUpdateRegionEvent
import net.dv8tion.jda.client.events.call.update.CallUpdateRingingUsersEvent
import net.dv8tion.jda.client.events.call.update.GenericCallUpdateEvent
import net.dv8tion.jda.client.events.call.voice.*
import net.dv8tion.jda.client.events.group.*
import net.dv8tion.jda.client.events.group.update.GenericGroupUpdateEvent
import net.dv8tion.jda.client.events.group.update.GroupUpdateIconEvent
import net.dv8tion.jda.client.events.group.update.GroupUpdateNameEvent
import net.dv8tion.jda.client.events.group.update.GroupUpdateOwnerEvent
import net.dv8tion.jda.client.events.message.group.*
import net.dv8tion.jda.client.events.message.group.react.GenericGroupMessageReactionEvent
import net.dv8tion.jda.client.events.message.group.react.GroupMessageReactionAddEvent
import net.dv8tion.jda.client.events.message.group.react.GroupMessageReactionRemoveAllEvent
import net.dv8tion.jda.client.events.message.group.react.GroupMessageReactionRemoveEvent
import net.dv8tion.jda.client.events.relationship.*
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.events.*
import net.dv8tion.jda.core.events.channel.category.CategoryCreateEvent
import net.dv8tion.jda.core.events.channel.category.CategoryDeleteEvent
import net.dv8tion.jda.core.events.channel.category.GenericCategoryEvent
import net.dv8tion.jda.core.events.channel.category.update.CategoryUpdateNameEvent
import net.dv8tion.jda.core.events.channel.category.update.CategoryUpdatePermissionsEvent
import net.dv8tion.jda.core.events.channel.category.update.CategoryUpdatePositionEvent
import net.dv8tion.jda.core.events.channel.category.update.GenericCategoryUpdateEvent
import net.dv8tion.jda.core.events.channel.priv.PrivateChannelCreateEvent
import net.dv8tion.jda.core.events.channel.priv.PrivateChannelDeleteEvent
import net.dv8tion.jda.core.events.channel.text.GenericTextChannelEvent
import net.dv8tion.jda.core.events.channel.text.TextChannelCreateEvent
import net.dv8tion.jda.core.events.channel.text.TextChannelDeleteEvent
import net.dv8tion.jda.core.events.channel.text.update.*
import net.dv8tion.jda.core.events.channel.voice.GenericVoiceChannelEvent
import net.dv8tion.jda.core.events.channel.voice.VoiceChannelCreateEvent
import net.dv8tion.jda.core.events.channel.voice.VoiceChannelDeleteEvent
import net.dv8tion.jda.core.events.channel.voice.update.*
import net.dv8tion.jda.core.events.emote.EmoteAddedEvent
import net.dv8tion.jda.core.events.emote.EmoteRemovedEvent
import net.dv8tion.jda.core.events.emote.GenericEmoteEvent
import net.dv8tion.jda.core.events.emote.update.EmoteUpdateNameEvent
import net.dv8tion.jda.core.events.emote.update.EmoteUpdateRolesEvent
import net.dv8tion.jda.core.events.emote.update.GenericEmoteUpdateEvent
import net.dv8tion.jda.core.events.guild.*
import net.dv8tion.jda.core.events.guild.member.*
import net.dv8tion.jda.core.events.guild.update.*
import net.dv8tion.jda.core.events.guild.voice.*
import net.dv8tion.jda.core.events.http.HttpRequestEvent
import net.dv8tion.jda.core.events.message.*
import net.dv8tion.jda.core.events.message.guild.*
import net.dv8tion.jda.core.events.message.guild.react.GenericGuildMessageReactionEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveAllEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.core.events.message.priv.*
import net.dv8tion.jda.core.events.message.priv.react.GenericPrivateMessageReactionEvent
import net.dv8tion.jda.core.events.message.priv.react.PrivateMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.priv.react.PrivateMessageReactionRemoveEvent
import net.dv8tion.jda.core.events.message.react.GenericMessageReactionEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveAllEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent
import net.dv8tion.jda.core.events.role.GenericRoleEvent
import net.dv8tion.jda.core.events.role.RoleCreateEvent
import net.dv8tion.jda.core.events.role.RoleDeleteEvent
import net.dv8tion.jda.core.events.role.update.*
import net.dv8tion.jda.core.events.self.*
import net.dv8tion.jda.core.events.user.GenericUserEvent
import net.dv8tion.jda.core.events.user.UserTypingEvent
import net.dv8tion.jda.core.events.user.update.*
import net.dv8tion.jda.core.hooks.EventListener

open class AsyncListenerAdapter : EventListener {

    open suspend fun onGenericEvent(event: Event) {}

    open suspend fun onGenericUpdate(event: UpdateEvent<*, *>) {}

    open suspend fun onReady(event: ReadyEvent) {}

    open suspend fun onResume(event: ResumedEvent) {}

    open suspend fun onReconnect(event: ReconnectedEvent) {}

    open suspend fun onDisconnect(event: DisconnectEvent) {}

    open suspend fun onShutdown(event: ShutdownEvent) {}

    open suspend fun onStatusChange(event: StatusChangeEvent) {}

    open suspend fun onException(event: ExceptionEvent) {}

    open suspend fun onUserUpdateName(event: UserUpdateNameEvent) {}

    open suspend fun onUserUpdateDiscriminator(event: UserUpdateDiscriminatorEvent) {}

    open suspend fun onUserUpdateAvatar(event: UserUpdateAvatarEvent) {}

    open suspend fun onUserUpdateOnlineStatus(event: UserUpdateOnlineStatusEvent) {}

    open suspend fun onUserUpdateGame(event: UserUpdateGameEvent) {}

    open suspend fun onUserTyping(event: UserTypingEvent) {}

    open suspend fun onSelfUpdateAvatar(event: SelfUpdateAvatarEvent) {}

    open suspend fun onSelfUpdateEmail(event: SelfUpdateEmailEvent) {}

    open suspend fun onSelfUpdateMFA(event: SelfUpdateMFAEvent) {}

    open suspend fun onSelfUpdateName(event: SelfUpdateNameEvent) {}

    open suspend fun onSelfUpdateVerified(event: SelfUpdateVerifiedEvent) {}

    open suspend fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {}

    open suspend fun onGuildMessageUpdate(event: GuildMessageUpdateEvent) {}

    open suspend fun onGuildMessageDelete(event: GuildMessageDeleteEvent) {}

    open suspend fun onGuildMessageEmbed(event: GuildMessageEmbedEvent) {}

    open suspend fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {}

    open suspend fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {}

    open suspend fun onGuildMessageReactionRemoveAll(event: GuildMessageReactionRemoveAllEvent) {}

    open suspend fun onPrivateMessageReceived(event: PrivateMessageReceivedEvent) {}

    open suspend fun onPrivateMessageUpdate(event: PrivateMessageUpdateEvent) {}

    open suspend fun onPrivateMessageDelete(event: PrivateMessageDeleteEvent) {}

    open suspend fun onPrivateMessageEmbed(event: PrivateMessageEmbedEvent) {}

    open suspend fun onPrivateMessageReactionAdd(event: PrivateMessageReactionAddEvent) {}

    open suspend fun onPrivateMessageReactionRemove(event: PrivateMessageReactionRemoveEvent) {}

    open suspend fun onMessageReceived(event: MessageReceivedEvent) {}

    open suspend fun onMessageUpdate(event: MessageUpdateEvent) {}

    open suspend fun onMessageDelete(event: MessageDeleteEvent) {}

    open suspend fun onMessageBulkDelete(event: MessageBulkDeleteEvent) {}

    open suspend fun onMessageEmbed(event: MessageEmbedEvent) {}

    open suspend fun onMessageReactionAdd(event: MessageReactionAddEvent) {}

    open suspend fun onMessageReactionRemove(event: MessageReactionRemoveEvent) {}

    open suspend fun onMessageReactionRemoveAll(event: MessageReactionRemoveAllEvent) {}

    open suspend fun onTextChannelDelete(event: TextChannelDeleteEvent) {}

    open suspend fun onTextChannelUpdateName(event: TextChannelUpdateNameEvent) {}

    open suspend fun onTextChannelUpdateTopic(event: TextChannelUpdateTopicEvent) {}

    open suspend fun onTextChannelUpdatePosition(event: TextChannelUpdatePositionEvent) {}

    open suspend fun onTextChannelUpdatePermissions(event: TextChannelUpdatePermissionsEvent) {}

    open suspend fun onTextChannelUpdateNSFW(event: TextChannelUpdateNSFWEvent) {}

    open suspend fun onTextChannelUpdateParent(event: TextChannelUpdateParentEvent) {}

    open suspend fun onTextChannelUpdateSlowmode(event: TextChannelUpdateSlowmodeEvent) {}

    open suspend fun onTextChannelCreate(event: TextChannelCreateEvent) {}

    open suspend fun onVoiceChannelDelete(event: VoiceChannelDeleteEvent) {}

    open suspend fun onVoiceChannelUpdateName(event: VoiceChannelUpdateNameEvent) {}

    open suspend fun onVoiceChannelUpdatePosition(event: VoiceChannelUpdatePositionEvent) {}

    open suspend fun onVoiceChannelUpdateUserLimit(event: VoiceChannelUpdateUserLimitEvent) {}

    open suspend fun onVoiceChannelUpdateBitrate(event: VoiceChannelUpdateBitrateEvent) {}

    open suspend fun onVoiceChannelUpdatePermissions(event: VoiceChannelUpdatePermissionsEvent) {}

    open suspend fun onVoiceChannelUpdateParent(event: VoiceChannelUpdateParentEvent) {}

    open suspend fun onVoiceChannelCreate(event: VoiceChannelCreateEvent) {}

    open suspend fun onCategoryDelete(event: CategoryDeleteEvent) {}

    open suspend fun onCategoryUpdateName(event: CategoryUpdateNameEvent) {}

    open suspend fun onCategoryUpdatePosition(event: CategoryUpdatePositionEvent) {}

    open suspend fun onCategoryUpdatePermissions(event: CategoryUpdatePermissionsEvent) {}

    open suspend fun onCategoryCreate(event: CategoryCreateEvent) {}

    open suspend fun onPrivateChannelCreate(event: PrivateChannelCreateEvent) {}

    open suspend fun onPrivateChannelDelete(event: PrivateChannelDeleteEvent) {}

    open suspend fun onGuildReady(event: GuildReadyEvent) {}

    open suspend fun onGuildJoin(event: GuildJoinEvent) {}

    open suspend fun onGuildLeave(event: GuildLeaveEvent) {}

    open suspend fun onGuildAvailable(event: GuildAvailableEvent) {}

    open suspend fun onGuildUnavailable(event: GuildUnavailableEvent) {}

    open suspend fun onUnavailableGuildJoined(event: UnavailableGuildJoinedEvent) {}

    open suspend fun onGuildBan(event: GuildBanEvent) {}

    open suspend fun onGuildUnban(event: GuildUnbanEvent) {}

    open suspend fun onGuildUpdateAfkChannel(event: GuildUpdateAfkChannelEvent) {}

    open suspend fun onGuildUpdateSystemChannel(event: GuildUpdateSystemChannelEvent) {}

    open suspend fun onGuildUpdateAfkTimeout(event: GuildUpdateAfkTimeoutEvent) {}

    open suspend fun onGuildUpdateExplicitContentLevel(event: GuildUpdateExplicitContentLevelEvent) {}

    open suspend fun onGuildUpdateIcon(event: GuildUpdateIconEvent) {}

    open suspend fun onGuildUpdateMFALevel(event: GuildUpdateMFALevelEvent) {}

    open suspend fun onGuildUpdateName(event: GuildUpdateNameEvent) {}

    open suspend fun onGuildUpdateNotificationLevel(event: GuildUpdateNotificationLevelEvent) {}

    open suspend fun onGuildUpdateOwner(event: GuildUpdateOwnerEvent) {}

    open suspend fun onGuildUpdateRegion(event: GuildUpdateRegionEvent) {}

    open suspend fun onGuildUpdateSplash(event: GuildUpdateSplashEvent) {}

    open suspend fun onGuildUpdateVerificationLevel(event: GuildUpdateVerificationLevelEvent) {}

    open suspend fun onGuildUpdateFeatures(event: GuildUpdateFeaturesEvent) {}

    open suspend fun onGuildMemberJoin(event: GuildMemberJoinEvent) {}

    open suspend fun onGuildMemberLeave(event: GuildMemberLeaveEvent) {}

    open suspend fun onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent) {}

    open suspend fun onGuildMemberRoleRemove(event: GuildMemberRoleRemoveEvent) {}

    open suspend fun onGuildMemberNickChange(event: GuildMemberNickChangeEvent) {}

    open suspend fun onGuildVoiceUpdate(event: GuildVoiceUpdateEvent) {}

    open suspend fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {}

    open suspend fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {}

    open suspend fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {}

    open suspend fun onGuildVoiceMute(event: GuildVoiceMuteEvent) {}

    open suspend fun onGuildVoiceDeafen(event: GuildVoiceDeafenEvent) {}

    open suspend fun onGuildVoiceGuildMute(event: GuildVoiceGuildMuteEvent) {}

    open suspend fun onGuildVoiceGuildDeafen(event: GuildVoiceGuildDeafenEvent) {}

    open suspend fun onGuildVoiceSelfMute(event: GuildVoiceSelfMuteEvent) {}

    open suspend fun onGuildVoiceSelfDeafen(event: GuildVoiceSelfDeafenEvent) {}

    open suspend fun onGuildVoiceSuppress(event: GuildVoiceSuppressEvent) {}

    open suspend fun onRoleCreate(event: RoleCreateEvent) {}

    open suspend fun onRoleDelete(event: RoleDeleteEvent) {}

    open suspend fun onRoleUpdateColor(event: RoleUpdateColorEvent) {}

    open suspend fun onRoleUpdateHoisted(event: RoleUpdateHoistedEvent) {}

    open suspend fun onRoleUpdateMentionable(event: RoleUpdateMentionableEvent) {}

    open suspend fun onRoleUpdateName(event: RoleUpdateNameEvent) {}

    open suspend fun onRoleUpdatePermissions(event: RoleUpdatePermissionsEvent) {}

    open suspend fun onRoleUpdatePosition(event: RoleUpdatePositionEvent) {}

    open suspend fun onEmoteAdded(event: EmoteAddedEvent) {}

    open suspend fun onEmoteRemoved(event: EmoteRemovedEvent) {}

    open suspend fun onEmoteUpdateName(event: EmoteUpdateNameEvent) {}

    open suspend fun onEmoteUpdateRoles(event: EmoteUpdateRolesEvent) {}

    open suspend fun onHttpRequest(event: HttpRequestEvent) {}

    open suspend fun onGenericMessage(event: GenericMessageEvent) {}

    open suspend fun onGenericMessageReaction(event: GenericMessageReactionEvent) {}

    open suspend fun onGenericGuildMessage(event: GenericGuildMessageEvent) {}

    open suspend fun onGenericGuildMessageReaction(event: GenericGuildMessageReactionEvent) {}

    open suspend fun onGenericPrivateMessage(event: GenericPrivateMessageEvent) {}

    open suspend fun onGenericPrivateMessageReaction(event: GenericPrivateMessageReactionEvent) {}

    open suspend fun onGenericUser(event: GenericUserEvent) {}

    open suspend fun onGenericUserPresence(event: GenericUserPresenceEvent<*>) {}

    open suspend fun onGenericSelfUpdate(event: GenericSelfUpdateEvent<*>) {}

    open suspend fun onGenericTextChannel(event: GenericTextChannelEvent) {}

    open suspend fun onGenericTextChannelUpdate(event: GenericTextChannelUpdateEvent<*>) {}

    open suspend fun onGenericVoiceChannel(event: GenericVoiceChannelEvent) {}

    open suspend fun onGenericVoiceChannelUpdate(event: GenericVoiceChannelUpdateEvent<*>) {}

    open suspend fun onGenericCategory(event: GenericCategoryEvent) {}

    open suspend fun onGenericCategoryUpdate(event: GenericCategoryUpdateEvent<*>) {}

    open suspend fun onGenericGuild(event: GenericGuildEvent) {}

    open suspend fun onGenericGuildUpdate(event: GenericGuildUpdateEvent<*>) {}

    open suspend fun onGenericGuildMember(event: GenericGuildMemberEvent) {}

    open suspend fun onGenericGuildVoice(event: GenericGuildVoiceEvent) {}

    open suspend fun onGenericRole(event: GenericRoleEvent) {}

    open suspend fun onGenericRoleUpdate(event: GenericRoleUpdateEvent<*>) {}

    open suspend fun onGenericEmote(event: GenericEmoteEvent) {}

    open suspend fun onGenericEmoteUpdate(event: GenericEmoteUpdateEvent<*>) {}

    open suspend fun onFriendAdded(event: FriendAddedEvent) {}

    open suspend fun onFriendRemoved(event: FriendRemovedEvent) {}

    open suspend fun onUserBlocked(event: UserBlockedEvent) {}

    open suspend fun onUserUnblocked(event: UserUnblockedEvent) {}

    open suspend fun onFriendRequestSent(event: FriendRequestSentEvent) {}

    open suspend fun onFriendRequestCanceled(event: FriendRequestCanceledEvent) {}

    open suspend fun onFriendRequestReceived(event: FriendRequestReceivedEvent) {}

    open suspend fun onFriendRequestIgnored(event: FriendRequestIgnoredEvent) {}

    open suspend fun onGroupJoin(event: GroupJoinEvent) {}

    open suspend fun onGroupLeave(event: GroupLeaveEvent) {}

    open suspend fun onGroupUserJoin(event: GroupUserJoinEvent) {}

    open suspend fun onGroupUserLeave(event: GroupUserLeaveEvent) {}

    open suspend fun onGroupMessageReceived(event: GroupMessageReceivedEvent) {}

    open suspend fun onGroupMessageUpdate(event: GroupMessageUpdateEvent) {}

    open suspend fun onGroupMessageDelete(event: GroupMessageDeleteEvent) {}

    open suspend fun onGroupMessageEmbed(event: GroupMessageEmbedEvent) {}

    open suspend fun onGroupMessageReactionAdd(event: GroupMessageReactionAddEvent) {}

    open suspend fun onGroupMessageReactionRemove(event: GroupMessageReactionRemoveEvent) {}

    open suspend fun onGroupMessageReactionRemoveAll(event: GroupMessageReactionRemoveAllEvent) {}

    open suspend fun onGroupUpdateIcon(event: GroupUpdateIconEvent) {}

    open suspend fun onGroupUpdateName(event: GroupUpdateNameEvent) {}

    open suspend fun onGroupUpdateOwner(event: GroupUpdateOwnerEvent) {}

    open suspend fun onCallCreate(event: CallCreateEvent) {}

    open suspend fun onCallDelete(event: CallDeleteEvent) {}

    open suspend fun onCallUpdateRegion(event: CallUpdateRegionEvent) {}

    open suspend fun onCallUpdateRingingUsers(event: CallUpdateRingingUsersEvent) {}

    open suspend fun onCallVoiceJoin(event: CallVoiceJoinEvent) {}

    open suspend fun onCallVoiceLeave(event: CallVoiceLeaveEvent) {}

    open suspend fun onCallVoiceSelfMute(event: CallVoiceSelfMuteEvent) {}

    open suspend fun onCallVoiceSelfDeafen(event: CallVoiceSelfDeafenEvent) {}

    open suspend fun onGenericRelationship(event: GenericRelationshipEvent) {}

    open suspend fun onGenericRelationshipAdd(event: GenericRelationshipAddEvent) {}

    open suspend fun onGenericRelationshipRemove(event: GenericRelationshipRemoveEvent) {}

    open suspend fun onGenericGroup(event: GenericGroupEvent) {}

    open suspend fun onGenericGroupMessage(event: GenericGroupMessageEvent) {}

    open suspend fun onGenericGroupMessageReaction(event: GenericGroupMessageReactionEvent) {}

    open suspend fun onGenericGroupUpdate(event: GenericGroupUpdateEvent) {}

    open suspend fun onGenericCall(event: GenericCallEvent) {}

    open suspend fun onGenericCallUpdate(event: GenericCallUpdateEvent) {}

    open suspend fun onGenericCallVoice(event: GenericCallVoiceEvent) {}

    override fun onEvent(event: Event) {
        runBlocking {
            this.run {
                this@AsyncListenerAdapter.onGenericEvent(event)
                if (event is UpdateEvent<*, *>) {
                    this@AsyncListenerAdapter.onGenericUpdate(event as UpdateEvent<*, *>)
                }
                when (event) {
                    is ReadyEvent -> this@AsyncListenerAdapter.onReady(event)
                    is ResumedEvent -> this@AsyncListenerAdapter.onResume(event)
                    is ReconnectedEvent -> this@AsyncListenerAdapter.onReconnect(event)
                    is DisconnectEvent -> this@AsyncListenerAdapter.onDisconnect(event)
                    is ShutdownEvent -> this@AsyncListenerAdapter.onShutdown(event)
                    is StatusChangeEvent -> this@AsyncListenerAdapter.onStatusChange(event)
                    is ExceptionEvent -> this@AsyncListenerAdapter.onException(event)
                    is GuildMessageReceivedEvent -> this@AsyncListenerAdapter.onGuildMessageReceived(event)
                    is GuildMessageUpdateEvent -> this@AsyncListenerAdapter.onGuildMessageUpdate(event)
                    is GuildMessageDeleteEvent -> this@AsyncListenerAdapter.onGuildMessageDelete(event)
                    is GuildMessageEmbedEvent -> this@AsyncListenerAdapter.onGuildMessageEmbed(event)
                    is GuildMessageReactionAddEvent -> this@AsyncListenerAdapter.onGuildMessageReactionAdd(event)
                    is GuildMessageReactionRemoveEvent -> this@AsyncListenerAdapter.onGuildMessageReactionRemove(event)
                    is GuildMessageReactionRemoveAllEvent -> this@AsyncListenerAdapter.onGuildMessageReactionRemoveAll(event)
                    is PrivateMessageReceivedEvent -> this@AsyncListenerAdapter.onPrivateMessageReceived(event)
                    is PrivateMessageUpdateEvent -> this@AsyncListenerAdapter.onPrivateMessageUpdate(event)
                    is PrivateMessageDeleteEvent -> this@AsyncListenerAdapter.onPrivateMessageDelete(event)
                    is PrivateMessageEmbedEvent -> this@AsyncListenerAdapter.onPrivateMessageEmbed(event)
                    is PrivateMessageReactionAddEvent -> this@AsyncListenerAdapter.onPrivateMessageReactionAdd(event)
                    is PrivateMessageReactionRemoveEvent -> this@AsyncListenerAdapter.onPrivateMessageReactionRemove(event)
                    is MessageReceivedEvent -> this@AsyncListenerAdapter.onMessageReceived(event)
                    is MessageUpdateEvent -> this@AsyncListenerAdapter.onMessageUpdate(event)
                    is MessageDeleteEvent -> this@AsyncListenerAdapter.onMessageDelete(event)
                    is MessageBulkDeleteEvent -> this@AsyncListenerAdapter.onMessageBulkDelete(event)
                    is MessageEmbedEvent -> this@AsyncListenerAdapter.onMessageEmbed(event)
                    is MessageReactionAddEvent -> this@AsyncListenerAdapter.onMessageReactionAdd(event)
                    is MessageReactionRemoveEvent -> this@AsyncListenerAdapter.onMessageReactionRemove(event)
                    is MessageReactionRemoveAllEvent -> this@AsyncListenerAdapter.onMessageReactionRemoveAll(event)
                    is UserUpdateNameEvent -> this@AsyncListenerAdapter.onUserUpdateName(event)
                    is UserUpdateDiscriminatorEvent -> this@AsyncListenerAdapter.onUserUpdateDiscriminator(event)
                    is UserUpdateAvatarEvent -> this@AsyncListenerAdapter.onUserUpdateAvatar(event)
                    is UserUpdateGameEvent -> this@AsyncListenerAdapter.onUserUpdateGame(event)
                    is UserUpdateOnlineStatusEvent -> this@AsyncListenerAdapter.onUserUpdateOnlineStatus(event)
                    is UserTypingEvent -> this@AsyncListenerAdapter.onUserTyping(event)
                    is SelfUpdateAvatarEvent -> this@AsyncListenerAdapter.onSelfUpdateAvatar(event)
                    is SelfUpdateEmailEvent -> this@AsyncListenerAdapter.onSelfUpdateEmail(event)
                    is SelfUpdateMFAEvent -> this@AsyncListenerAdapter.onSelfUpdateMFA(event)
                    is SelfUpdateNameEvent -> this@AsyncListenerAdapter.onSelfUpdateName(event)
                    is SelfUpdateVerifiedEvent -> this@AsyncListenerAdapter.onSelfUpdateVerified(event)
                    is TextChannelCreateEvent -> this@AsyncListenerAdapter.onTextChannelCreate(event)
                    is TextChannelUpdateNameEvent -> this@AsyncListenerAdapter.onTextChannelUpdateName(event)
                    is TextChannelUpdateTopicEvent -> this@AsyncListenerAdapter.onTextChannelUpdateTopic(event)
                    is TextChannelUpdatePositionEvent -> this@AsyncListenerAdapter.onTextChannelUpdatePosition(event)
                    is TextChannelUpdatePermissionsEvent -> this@AsyncListenerAdapter.onTextChannelUpdatePermissions(event)
                    is TextChannelUpdateNSFWEvent -> this@AsyncListenerAdapter.onTextChannelUpdateNSFW(event)
                    is TextChannelUpdateParentEvent -> this@AsyncListenerAdapter.onTextChannelUpdateParent(event)
                    is TextChannelUpdateSlowmodeEvent -> this@AsyncListenerAdapter.onTextChannelUpdateSlowmode(event)
                    is TextChannelDeleteEvent -> this@AsyncListenerAdapter.onTextChannelDelete(event)
                    is VoiceChannelCreateEvent -> this@AsyncListenerAdapter.onVoiceChannelCreate(event)
                    is VoiceChannelUpdateNameEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdateName(event)
                    is VoiceChannelUpdatePositionEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdatePosition(event)
                    is VoiceChannelUpdateUserLimitEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdateUserLimit(event)
                    is VoiceChannelUpdateBitrateEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdateBitrate(event)
                    is VoiceChannelUpdatePermissionsEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdatePermissions(event)
                    is VoiceChannelUpdateParentEvent -> this@AsyncListenerAdapter.onVoiceChannelUpdateParent(event)
                    is VoiceChannelDeleteEvent -> this@AsyncListenerAdapter.onVoiceChannelDelete(event)
                    is CategoryCreateEvent -> this@AsyncListenerAdapter.onCategoryCreate(event)
                    is CategoryUpdateNameEvent -> this@AsyncListenerAdapter.onCategoryUpdateName(event)
                    is CategoryUpdatePositionEvent -> this@AsyncListenerAdapter.onCategoryUpdatePosition(event)
                    is CategoryUpdatePermissionsEvent -> this@AsyncListenerAdapter.onCategoryUpdatePermissions(event)
                    is CategoryDeleteEvent -> this@AsyncListenerAdapter.onCategoryDelete(event)
                    is PrivateChannelCreateEvent -> this@AsyncListenerAdapter.onPrivateChannelCreate(event)
                    is PrivateChannelDeleteEvent -> this@AsyncListenerAdapter.onPrivateChannelDelete(event)
                    is GuildReadyEvent -> this@AsyncListenerAdapter.onGuildReady(event)
                    is GuildJoinEvent -> this@AsyncListenerAdapter.onGuildJoin(event)
                    is GuildLeaveEvent -> this@AsyncListenerAdapter.onGuildLeave(event)
                    is GuildAvailableEvent -> this@AsyncListenerAdapter.onGuildAvailable(event)
                    is GuildUnavailableEvent -> this@AsyncListenerAdapter.onGuildUnavailable(event)
                    is UnavailableGuildJoinedEvent -> this@AsyncListenerAdapter.onUnavailableGuildJoined(event)
                    is GuildBanEvent -> this@AsyncListenerAdapter.onGuildBan(event)
                    is GuildUnbanEvent -> this@AsyncListenerAdapter.onGuildUnban(event)
                    is GuildUpdateAfkChannelEvent -> this@AsyncListenerAdapter.onGuildUpdateAfkChannel(event)
                    is GuildUpdateSystemChannelEvent -> this@AsyncListenerAdapter.onGuildUpdateSystemChannel(event)
                    is GuildUpdateAfkTimeoutEvent -> this@AsyncListenerAdapter.onGuildUpdateAfkTimeout(event)
                    is GuildUpdateExplicitContentLevelEvent -> this@AsyncListenerAdapter.onGuildUpdateExplicitContentLevel(event)
                    is GuildUpdateIconEvent -> this@AsyncListenerAdapter.onGuildUpdateIcon(event)
                    is GuildUpdateMFALevelEvent -> this@AsyncListenerAdapter.onGuildUpdateMFALevel(event)
                    is GuildUpdateNameEvent -> this@AsyncListenerAdapter.onGuildUpdateName(event)
                    is GuildUpdateNotificationLevelEvent -> this@AsyncListenerAdapter.onGuildUpdateNotificationLevel(event)
                    is GuildUpdateOwnerEvent -> this@AsyncListenerAdapter.onGuildUpdateOwner(event)
                    is GuildUpdateRegionEvent -> this@AsyncListenerAdapter.onGuildUpdateRegion(event)
                    is GuildUpdateSplashEvent -> this@AsyncListenerAdapter.onGuildUpdateSplash(event)
                    is GuildUpdateVerificationLevelEvent -> this@AsyncListenerAdapter.onGuildUpdateVerificationLevel(event)
                    is GuildUpdateFeaturesEvent -> this@AsyncListenerAdapter.onGuildUpdateFeatures(event)
                    is GuildMemberJoinEvent -> this@AsyncListenerAdapter.onGuildMemberJoin(event)
                    is GuildMemberLeaveEvent -> this@AsyncListenerAdapter.onGuildMemberLeave(event)
                    is GuildMemberRoleAddEvent -> this@AsyncListenerAdapter.onGuildMemberRoleAdd(event)
                    is GuildMemberRoleRemoveEvent -> this@AsyncListenerAdapter.onGuildMemberRoleRemove(event)
                    is GuildMemberNickChangeEvent -> this@AsyncListenerAdapter.onGuildMemberNickChange(event)
                    is GuildVoiceJoinEvent -> this@AsyncListenerAdapter.onGuildVoiceJoin(event)
                    is GuildVoiceMoveEvent -> this@AsyncListenerAdapter.onGuildVoiceMove(event)
                    is GuildVoiceLeaveEvent -> this@AsyncListenerAdapter.onGuildVoiceLeave(event)
                    is GuildVoiceMuteEvent -> this@AsyncListenerAdapter.onGuildVoiceMute(event)
                    is GuildVoiceDeafenEvent -> this@AsyncListenerAdapter.onGuildVoiceDeafen(event)
                    is GuildVoiceGuildMuteEvent -> this@AsyncListenerAdapter.onGuildVoiceGuildMute(event)
                    is GuildVoiceGuildDeafenEvent -> this@AsyncListenerAdapter.onGuildVoiceGuildDeafen(event)
                    is GuildVoiceSelfMuteEvent -> this@AsyncListenerAdapter.onGuildVoiceSelfMute(event)
                    is GuildVoiceSelfDeafenEvent -> this@AsyncListenerAdapter.onGuildVoiceSelfDeafen(event)
                    is GuildVoiceSuppressEvent -> this@AsyncListenerAdapter.onGuildVoiceSuppress(event)
                    is RoleCreateEvent -> this@AsyncListenerAdapter.onRoleCreate(event)
                    is RoleDeleteEvent -> this@AsyncListenerAdapter.onRoleDelete(event)
                    is RoleUpdateColorEvent -> this@AsyncListenerAdapter.onRoleUpdateColor(event)
                    is RoleUpdateHoistedEvent -> this@AsyncListenerAdapter.onRoleUpdateHoisted(event)
                    is RoleUpdateMentionableEvent -> this@AsyncListenerAdapter.onRoleUpdateMentionable(event)
                    is RoleUpdateNameEvent -> this@AsyncListenerAdapter.onRoleUpdateName(event)
                    is RoleUpdatePermissionsEvent -> this@AsyncListenerAdapter.onRoleUpdatePermissions(event)
                    is RoleUpdatePositionEvent -> this@AsyncListenerAdapter.onRoleUpdatePosition(event)
                    is EmoteAddedEvent -> this@AsyncListenerAdapter.onEmoteAdded(event)
                    is EmoteRemovedEvent -> this@AsyncListenerAdapter.onEmoteRemoved(event)
                    is EmoteUpdateNameEvent -> this@AsyncListenerAdapter.onEmoteUpdateName(event)
                    is EmoteUpdateRolesEvent -> this@AsyncListenerAdapter.onEmoteUpdateRoles(event)
                    is HttpRequestEvent -> this@AsyncListenerAdapter.onHttpRequest(event)
                }

                if (event is GuildVoiceUpdateEvent) {
                    this@AsyncListenerAdapter.onGuildVoiceUpdate(event)
                }

                when (event) {
                    is GenericMessageReactionEvent -> this@AsyncListenerAdapter.onGenericMessageReaction(event)
                    is GenericPrivateMessageReactionEvent -> this@AsyncListenerAdapter.onGenericPrivateMessageReaction(event)
                    is GenericTextChannelUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericTextChannelUpdate(event)
                    is GenericCategoryUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericCategoryUpdate(event)
                    is GenericGuildMessageReactionEvent -> this@AsyncListenerAdapter.onGenericGuildMessageReaction(event)
                    is GenericVoiceChannelUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericVoiceChannelUpdate(event)
                    is GenericGuildUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericGuildUpdate(event)
                    is GenericGuildMemberEvent -> this@AsyncListenerAdapter.onGenericGuildMember(event)
                    is GenericGuildVoiceEvent -> this@AsyncListenerAdapter.onGenericGuildVoice(event)
                    is GenericRoleUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericRoleUpdate(event)
                    is GenericEmoteUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericEmoteUpdate(event)
                    is GenericUserPresenceEvent<*> -> this@AsyncListenerAdapter.onGenericUserPresence(event)
                }

                when (event) {
                    is GenericMessageEvent -> this@AsyncListenerAdapter.onGenericMessage(event)
                    is GenericPrivateMessageEvent -> this@AsyncListenerAdapter.onGenericPrivateMessage(event)
                    is GenericGuildMessageEvent -> this@AsyncListenerAdapter.onGenericGuildMessage(event)
                    is GenericUserEvent -> this@AsyncListenerAdapter.onGenericUser(event)
                    is GenericSelfUpdateEvent<*> -> this@AsyncListenerAdapter.onGenericSelfUpdate(event)
                    is GenericTextChannelEvent -> this@AsyncListenerAdapter.onGenericTextChannel(event)
                    is GenericVoiceChannelEvent -> this@AsyncListenerAdapter.onGenericVoiceChannel(event)
                    is GenericCategoryEvent -> this@AsyncListenerAdapter.onGenericCategory(event)
                    is GenericRoleEvent -> this@AsyncListenerAdapter.onGenericRole(event)
                    is GenericEmoteEvent -> this@AsyncListenerAdapter.onGenericEmote(event)
                }

                if (event is GenericGuildEvent) {
                    this@AsyncListenerAdapter.onGenericGuild(event)
                }

                if (event.jda.accountType == AccountType.CLIENT) {
                    when (event) {
                        is FriendAddedEvent -> this@AsyncListenerAdapter.onFriendAdded(event)
                        is FriendRemovedEvent -> this@AsyncListenerAdapter.onFriendRemoved(event)
                        is UserBlockedEvent -> this@AsyncListenerAdapter.onUserBlocked(event)
                        is UserUnblockedEvent -> this@AsyncListenerAdapter.onUserUnblocked(event)
                        is FriendRequestSentEvent -> this@AsyncListenerAdapter.onFriendRequestSent(event)
                        is FriendRequestCanceledEvent -> this@AsyncListenerAdapter.onFriendRequestCanceled(event)
                        is FriendRequestReceivedEvent -> this@AsyncListenerAdapter.onFriendRequestReceived(event)
                        is FriendRequestIgnoredEvent -> this@AsyncListenerAdapter.onFriendRequestIgnored(event)
                        is GroupJoinEvent -> this@AsyncListenerAdapter.onGroupJoin(event)
                        is GroupLeaveEvent -> this@AsyncListenerAdapter.onGroupLeave(event)
                        is GroupUserJoinEvent -> this@AsyncListenerAdapter.onGroupUserJoin(event)
                        is GroupUserLeaveEvent -> this@AsyncListenerAdapter.onGroupUserLeave(event)
                    }

                    when (event) {
                        is GroupMessageReceivedEvent -> this@AsyncListenerAdapter.onGroupMessageReceived(event)
                        is GroupMessageUpdateEvent -> this@AsyncListenerAdapter.onGroupMessageUpdate(event)
                        is GroupMessageDeleteEvent -> this@AsyncListenerAdapter.onGroupMessageDelete(event)
                        is GroupMessageEmbedEvent -> this@AsyncListenerAdapter.onGroupMessageEmbed(event)
                        is GroupMessageReactionAddEvent -> this@AsyncListenerAdapter.onGroupMessageReactionAdd(event)
                        is GroupMessageReactionRemoveEvent -> this@AsyncListenerAdapter.onGroupMessageReactionRemove(event)
                        is GroupMessageReactionRemoveAllEvent -> this@AsyncListenerAdapter.onGroupMessageReactionRemoveAll(event)
                        is GroupUpdateIconEvent -> this@AsyncListenerAdapter.onGroupUpdateIcon(event)
                        is GroupUpdateNameEvent -> this@AsyncListenerAdapter.onGroupUpdateName(event)
                        is GroupUpdateOwnerEvent -> this@AsyncListenerAdapter.onGroupUpdateOwner(event)
                        is CallCreateEvent -> this@AsyncListenerAdapter.onCallCreate(event)
                        is CallDeleteEvent -> this@AsyncListenerAdapter.onCallDelete(event)
                        is CallUpdateRegionEvent -> this@AsyncListenerAdapter.onCallUpdateRegion(event)
                        is CallUpdateRingingUsersEvent -> this@AsyncListenerAdapter.onCallUpdateRingingUsers(event)
                        is CallVoiceJoinEvent -> this@AsyncListenerAdapter.onCallVoiceJoin(event)
                        is CallVoiceLeaveEvent -> this@AsyncListenerAdapter.onCallVoiceLeave(event)
                        is CallVoiceSelfMuteEvent -> this@AsyncListenerAdapter.onCallVoiceSelfMute(event)
                        is CallVoiceSelfDeafenEvent -> this@AsyncListenerAdapter.onCallVoiceSelfDeafen(event)
                    }

                    when (event) {
                        is GenericRelationshipAddEvent -> this@AsyncListenerAdapter.onGenericRelationshipAdd(event)
                        is GenericRelationshipRemoveEvent -> this@AsyncListenerAdapter.onGenericRelationshipRemove(event)
                        is GenericGroupMessageReactionEvent -> this@AsyncListenerAdapter.onGenericGroupMessageReaction(event)
                        is GenericGroupUpdateEvent -> this@AsyncListenerAdapter.onGenericGroupUpdate(event)
                        is GenericCallUpdateEvent -> this@AsyncListenerAdapter.onGenericCallUpdate(event)
                        is GenericCallVoiceEvent -> this@AsyncListenerAdapter.onGenericCallVoice(event)
                    }

                    if (event is GenericGroupMessageEvent) {
                        this@AsyncListenerAdapter.onGenericGroupMessage(event)
                    }

                    when (event) {
                        is GenericRelationshipEvent -> this@AsyncListenerAdapter.onGenericRelationship(event)
                        is GenericGroupEvent -> this@AsyncListenerAdapter.onGenericGroup(event)
                        is GenericCallEvent -> this@AsyncListenerAdapter.onGenericCall(event)
                    }
                }
            }
        }
    }
}
